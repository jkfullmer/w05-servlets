package com.example.tomcatpractice;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String fname = request.getParameter("firstname");
        String lname = request.getParameter("lastname");
        String email = request.getParameter("email");
        out.println("<h1>Request successful!</h1>");
        out.println("<p>Thank you " + fname + ". Your information has been saved successfully.</p>");
        out.println("<p>You name: " + fname + " " + lname + "</p>");
        out.println("<p>You email: " + email + "</p>");
        out.println("</body></html>");
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Not in use.");
    }
}
